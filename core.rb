require '../config.rb'

require 'logger'
log = Logger.new("#{BASEDIR}/#{PROJECT_NAME}.log")

begin
  require 'cgi'
  require 'cgi/session'
  require 'cgi/session/pstore'
  require 'rubygems'
  require 'liquid'
  require 'htmlentities'
rescue LoadError => e
  log.error 'one or more required gems are not installed: ' + e.message
  exit
end

class Core
  @@cgi = nil
  @@session = nil
  @@prefs = nil
  @@uid = nil

  def self.render(view, data)
    Core.render_dynamic(File.open("#{BASEDIR}/views/#{view}.html", 'rb').read, data)
  end

  def self.render_dynamic(template, data)
    @@cgi = CGI.new unless @@cgi
    template = Liquid::Template.parse(template)
    page = (ENV['REQUEST_URI'] || ENV['_'] || '/index.rb')
    page = page.split('/').last if page.split('/')
    page = page.split('.').first if page.split('.')
    @@cgi.out {
      begin
        template.render(data.merge!({'core' => {
          'name' => PROJECT_NAME,
          'site_url' => SITE_URL,
          'base_url' => BASE_URL,
          'version' => VERSION,
          'page' => page
        }}))
      rescue Exception => e
        Core.log "Rendering exception when trying to render #{template} with #{data}"
        Core.log(e)
      end
    }
  end

  def self.log(message)
    log = Logger.new("#{BASEDIR}/#{PROJECT_NAME}.log") unless (log)
    log.error(message);
  end

  def self.session_get(index)
    @@cgi = CGI.new unless @@cgi
    @@session = CGI::Session.new(@@cgi, 'database_manager' => CGI::Session::PStore) unless (@@session)
    return @@session[index]
  end

  def self.session_put(index, value)
    @@cgi = CGI.new unless @@cgi
    @@session = CGI::Session.new(@@cgi, 'database_manager' => CGI::Session::PStore) unless (@@session)
    @@session[index] = value
  end

  def self.input(index)
    @@cgi = CGI.new unless @@cgi
    return false unless @@cgi.has_key? index
    result = @@cgi.params[index]
    result.strip! if result.is_a? String
    result = result.first if result.is_a? Array and result.length == 1
    return false if result.empty?
    return result
  end

  def self.post?
    @@cgi = CGI.new unless @@cgi
    return !@@cgi.params.empty?
  end

  # Returns the user ID of the currently logged in user.
  # Returns nil if authorization fails.
  def self.auth(username=nil, password=nil)
    @@uid = Core.session_get('uid') unless @@uid
    if username and password then
      user = Users.auth username, password
    elsif @@uid then
      user = Users.get @@uid
    else
      Core.session_put('uid', nil)
      Core.session_put('prefs', nil)
      return false
    end
    return nil if !user
    Core.session_put('uid', user['uid'])
    Core.session_put('prefs', user['prefs'])
    return user['uid']
  end

  def self.pref(prefname)
    if !@@prefs then
      Core.auth
      if !@@prefs then
        # TODO warn without being so damn annoying
        #Core.log "Tried to acces pref #{prefname} without being logged in."
        return false
      end
    end
    # TODO: figure out good way to set default prefs
    return @@prefs[prefname] if @@prefs.has_key? prefname
    return ''
  end

  def self.validate_date_format(date)
    return false unless date
    return false unless date =~ /\d{4}-\d\d-\d\d/
    parts = date.split '-'
    year = parts[0].to_i
    month = parts[0].to_i
    day = parts[0].to_i
    return false unless year >= 1970 and year <= 2038
    return false unless month >= 1 and month <= 12
    return false unless day >= 1 and day <= 31
    return true
  end

  def self.encode(string)
    return HTMLEntities.new.encode string, :named, :decimal
  end
end

#################################
# Liquid customizations go here #
#################################

class NavFooter < Liquid::Tag
  def render(context)
    template = Liquid::Template.parse(File.open("#{Core::BASEDIR}/views/components/nav.html", 'rb').read)
    page = (ENV['REQUEST_URI'] || ENV['_'] || '/index.rb')
    page = page.split('/').last if page.split('/')
    page = page.split('.').first if page.split('.')
    template.render({'page' => page})
  end
end

class Notification < Liquid::Tag
  def render(context)
    # TODO make this work
    #template = Liquid::Template.parse(File.open("#{Core::BASEDIR}/views/components/notification.html", 'rb').read)
    #template.render({'page' => ENV['REQUEST_URI'].split('/').last.split('.').first})
  end
end

Liquid::Template.register_tag('nav_footer', NavFooter)

##########################
# Monkey patches go here #
##########################

class Numeric
    def round_to(places)
        power = 10.0**places
        (self * power).round / power
    end
end
